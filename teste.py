from bs4 import BeautifulSoup
from textblob import TextBlob
from datetime import datetime
import requests
import csv # pra usar as funções de escrever uma csv file
import matplotlib.pyplot as plt

source = requests.get('https://www.bbc.co.uk/search?q=elon+musk').text # como fazer requests pra pegar os dados de uma URL e ainda em formato texto

soup = BeautifulSoup(source, 'html5lib') # como parametros leva a fonte daonde está pegando os dados e o tipo de parser, existem outros

# csv_file = open('tanto_faz.csv', 'w') # cria e abre uma file pra escrita dos dados

# csv_writer = csv.writer(csv_file) # cria um escritor (writer) pra file
# csv_writer.writerow(['headline', 'summary', 'video_link']) # coloca os labels das colunas
cont = 1
dataList = list()
polarityList = list()
for match in soup.find_all('article'): # loop pra pegar todos os itens da fonte, que retorna um objeto com as tags e/ou classes do elemento
	try:
		headline = match.h1.a.text
		date = match.time.text
		date2 = date.lstrip().rstrip() # você filtra o que quiser dela, que elemento quiser da file
		# dt = parser.parser(date.lstrip())
		blob = TextBlob(headline)
		polarity = blob.sentiment.polarity
		dataCerta = datetime.strptime(date2, '%d %b %Y')
		print("Numero:",cont)
		print("Date:",date2, "bababbba")
		print("Healine: ", headline)
		print("Polarity", polarity)
		print("date parsed", type(dataCerta))
		dataList.append(dataCerta.date())
		dataList.sort()
		polarityList.insert(dataList.index(dataCerta.date()), polarity)
		cont +=1
		print()
		print()
	except Exception as e: # se o script não encontra especificamente o que procura, se de repente o elemento não tem o campo que você quer, isso mata o script
		print("algum erro") # esse Try/Except facilita colocando um dado None caso de alguma merda

	# csv_writer.writerow([full_article]) # escreve na file todos os dados na sequência colocada nos parametros
for i in dataList:
	print(i,dataList.index(i))

for j in polarityList:
	print(j)
# csv_file.close() # fecha a file depois que termina de escrever todos os elementos
plt.scatter(dataList, polarityList, label="polarity", color="red", marker="*", s=30)
plt.xlabel('Dates')
plt.ylabel('Polarity')
plt.title('Polarity throw days')
plt.show()
